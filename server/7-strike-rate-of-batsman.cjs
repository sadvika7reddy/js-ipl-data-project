let fs = require('fs')
const csvtojson = require('csvtojson');
const path = require('path');
let matchesPath = path.resolve(__dirname,'../data/matches.csv');
let deliveriesPath = path.resolve(__dirname,'../data/deliveries.csv');

csvtojson().fromFile(matchesPath).then((matchesPath)=>{

  csvtojson().fromFile(deliveriesPath).then((deliveriesFile)=>{
    let strikeRateResult=strikeRate(matchesPath,deliveriesFile);
    console.log(strikeRateResult)
    fs.writeFileSync('../public/output/7-strike-rate-of-batsman.json', JSON.stringify(strikeRateResult)); 
      
  })  
})
 
function strikeRate(matchesPath,deliveriesFile){
      let matchesSeason=matchesPath.reduce((accumulator,current)=>{
         accumulator[current.id]=current.season
         return accumulator
      },{})
      let batsman=deliveriesFile.reduce((accumulator,current)=>{
          let year=matchesSeason[current.match_id];
          //console.log(year);
          if(accumulator[current.batsman]){
            if(accumulator[year]){
              accumulator[current.batsman][year].total_runs=+parseInt(current.total_runs);
                accumulator[current.batsman][year].total_batsman+=1;
                accumulator[current.batsman][year].strike=parseInt( accumulator[current.batsman][year].total_runs/accumulator[current.batsman][year].total_batsman)*100
            }
            else{
                accumulator[current.batsman][year]={}
                accumulator[current.batsman][year].total_runs=parseInt(current.total_runs);
                accumulator[current.batsman][year].total_batsman=1;
                accumulator[current.batsman][year].strike=parseInt( accumulator[current.batsman][year].total_runs/accumulator[current.batsman][year].total_batsman)*100
            }
          } 
          else{
            accumulator[current.batsman]={}
            accumulator[current.batsman][year]={}
            accumulator[current.batsman][year].total_runs=parseInt(current.total_runs);
            accumulator[current.batsman][year].total_batsman=1;
            accumulator[current.batsman][year].strike=parseInt( accumulator[current.batsman][year].total_runs/accumulator[current.batsman][year].total_batsman)*100 
        
          }
          return accumulator
      },{})
      return batsman;
      
}  