let fs = require('fs')
const csvtojson = require('csvtojson');
const path = require('path');
let matchesPath = path.resolve(__dirname,'../data/matches.csv');
let deliveriesPath = path.resolve(__dirname,'../data/deliveries.csv');

csvtojson().fromFile(matchesPath).then((matchesPath)=>{

  csvtojson().fromFile(deliveriesPath).then((deliveriesFile)=>{
    let extraRunsResult=extraRuns(matchesPath,deliveriesFile);
    //console.log(deliveriesFile)

    console.log(extraRunsResult);
    fs.writeFileSync('../public/output/3-extra-runs-conceded-per-year.json', JSON.stringify(extraRunsResult));
    
  })  
})
function extraRuns(matchesPath,deliveriesPath){
    let matchId=matchesPath.filter((value)=>{
        if(value.season=='2016')
        {
            return value.id;
        }
    }).map((item) => {
        return item.id
     })

    //console.log(matchId)
     return deliveriesPath.reduce((accumulator,current)=>{
          if(matchId.includes(current.match_id)){
                  if(accumulator[current.bowling_team]&&current.extra_runs!=undefined){
                     accumulator[current.bowling_team]+=parseInt(current.extra_runs);
                  }
                  else{
                        accumulator[current.bowling_team]=parseInt(current.extra_runs);
                  }
            
           }  
           return accumulator
     },{})


}

// function 
// function extraRuns2(result,deliveriesFile){
//     let path=deliveriesFile.reduce((accumulator,current,result)=>{
//         if(matchId.includes(current.match_id)) {
//         if(accumulator[current.match_id]==result.id){
//             accumulator[current.bowling_team]+=accumulator[current.extra_runs];
//         }
//     }
//     },{},deliveriesFile,result)
//     return path;
// }