const fs = require('fs');
const csvtojson = require('csvtojson');
const path = require('path');
let matchesPath = path.resolve(__dirname,'../data/matches.csv');

csvtojson().fromFile(matchesPath).then((matchesFile)=>{
    //console.log(matchesFile)
    let totalMatchesPerYear=totalMatches(matchesFile);
    console.log(totalMatchesPerYear)
    fs.writeFileSync('../public/output/1-matches-per-year.json', JSON.stringify(totalMatchesPerYear));
    
})
function totalMatches(Matches){
    let total=Matches.reduce((accumulator,current)=>{
        if(accumulator[current.season]){
            accumulator[current.season]++
        }
        else{
            accumulator[current.season]=1;
        }
        return accumulator;

},{})
return total;

}


