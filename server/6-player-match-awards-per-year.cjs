const fs = require('fs');
const csvtojson = require('csvtojson');
const path = require('path');
let matchesPath = path.resolve(__dirname,'../data/matches.csv');

csvtojson().fromFile(matchesPath).then((matchesFile)=>{
    //console.log(matchesFile)
    let matchesAwardResult=matchesAward(matchesFile);
    
    console.log(matchesAwardResult);
    fs.writeFileSync('../public/output/6-player-match-awards-per-year.json', JSON.stringify(matchesAwardResult));
    
})

function matchesAward(matchesFile){
    let numbers=matchesFile.reduce((accumulator,current)=>{
        if(accumulator[current.season]){
            if(accumulator[current.season][current.player_of_match]){
                accumulator[current.season][current.player_of_match]++;
            }
            else{
                accumulator[current.season][current.player_of_match]=1
            }   
        }
        else{
            accumulator[current.season]={};
            accumulator[current.season][current.player_of_match]=1;

        }
      return accumulator;
           
    },{})

    let answer = Object.entries(numbers).reduce((accumulator,[key1,item1]) => {
        // console.log(item2)
       let value=Object.entries(item1).sort((a,b) => {
           return b[1]-a[1]
       })[0]
              accumulator[key1]={
                "batsman": value[0],
                "awards":value[1]
              }
              return accumulator
    },{});
    // return answer;
    
    // let numbers1=numbers.filter((value)=>{
    //     let value1=0
        
    // })  
    // console.log(obj1)
    // 
    return answer;
    
}