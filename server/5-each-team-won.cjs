let fs = require('fs')
const csvtojson = require('csvtojson');
const path = require('path');
let matchesPath = path.resolve(__dirname,'../data/matches.csv');
let deliveriesPath = path.resolve(__dirname,'../data/deliveries.csv');

csvtojson().fromFile(matchesPath).then((matchesPath)=>{

  csvtojson().fromFile(deliveriesPath).then((deliveriesFile)=>{
    let teamWonresult=teamWon(matchesPath);
    //console.log(deliveriesFile)

    
    console.log(teamWonresult);
    fs.writeFileSync('../public/output/5-each-team-won.json', JSON.stringify(teamWonresult));
    
  })  
})
   
function teamWon(matchesPath){
    return matchesPath.reduce((accumulator,current)=>{
        if(current.winner===current.toss_winner){
            if(accumulator[current.winner]){
                accumulator[current.winner]++;
            }
            else{
                accumulator[current.winner]=1;
            }
        }
        return accumulator
    },{})
}
