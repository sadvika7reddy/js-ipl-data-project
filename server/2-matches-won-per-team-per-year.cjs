const fs = require('fs');
const csvtojson = require('csvtojson');
const path = require('path');
let matchesPath = path.resolve(__dirname,'../data/matches.csv');

csvtojson().fromFile(matchesPath).then((matchesFile)=>{
    //console.log(matchesFile)
    let matchesWonResult=matchesWon(matchesFile);
    console.log(matchesWonResult);
    fs.writeFileSync('../public/output/2-matches-won-per-team-per-year.json', JSON.stringify(matchesWonResult));
    
    
})
function matchesWon(matchesFile){
    let total=matchesFile.reduce((accumulator,current)=>{
            if(accumulator[current.season]){
                if(accumulator[current.season][current.winner]){
                    accumulator[current.season][current.winner]++;
                }
                else{
                    accumulator[current.season][current.winner]=1
                }   
            }
            else{
                accumulator[current.season]={};
                accumulator[current.season][current.winner]=1;

            }
          return accumulator;
    },{})
    return total;
}

 
