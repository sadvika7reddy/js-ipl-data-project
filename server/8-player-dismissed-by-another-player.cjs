let fs = require('fs')
const csvtojson = require('csvtojson');
const path = require('path');
let matchesPath = path.resolve(__dirname,'../data/matches.csv');
let deliveriesPath = path.resolve(__dirname,'../data/deliveries.csv');

csvtojson().fromFile(matchesPath).then((matchesPath)=>{

  csvtojson().fromFile(deliveriesPath).then((deliveriesFile)=>{
    let playerDismissedResult=playerDismissed(deliveriesFile);
    //console.log(deliveriesFile)

    
     console.log(playerDismissedResult);
     fs.writeFileSync('../public/output/8-player-dismissed-by-another-player.json', JSON.stringify(playerDismissedResult)); 

    
  })  
})
 function playerDismissed(deliveriesFile){
      let total=deliveriesFile.reduce((accumulator,current)=>{
         if(current.player_dismissed){ 
            if(accumulator[current.bowler]){
                if(accumulator[current.bowler][current.player_dismissed]){
                    accumulator[current.bowler][current.player_dismissed]++;
                }
                else{
                    accumulator[current.bowler][current.player_dismissed]=1
                }   
            }
            else{
                accumulator[current.bowler]={};
                accumulator[current.bowler][current.player_dismissed]=1;

            }
         }
         return accumulator
      },{})

      let answer = Object.entries(total).reduce((acc,[bowler,item]) => {
       const values= Object.entries(item).sort((a,b) => {
        return b[1] - a[1]
       })[0]
       acc[bowler] = {
         dissmissedPlayer : values[0],
         numberOfDismissals: values[1]
       }
       return acc;

    //    console.log(values)
      },{});
      
      
      return answer;
 }