let fs = require('fs')
const csvtojson = require('csvtojson');
const path = require('path');
let matchesPath = path.resolve(__dirname,'../data/matches.csv');
let deliveriesPath = path.resolve(__dirname,'../data/deliveries.csv');

csvtojson().fromFile(matchesPath).then((matchesPath)=>{

  csvtojson().fromFile(deliveriesPath).then((deliveriesFile)=>{
    let topBowlerResult=topBowler(matchesPath,deliveriesFile);
    //console.log(deliveriesFile)

    
    console.log(topBowlerResult);
    fs.writeFileSync('../public/output/4-10-economical-bowler.json', JSON.stringify(topBowlerResult));
    // let result2=extraRuns2(result,deliveriesFile);
    // console.log(result2)
    
  })  
})

function topBowler(matchesPath,deliveriesFile){
    let matchId=matchesPath.filter((value)=>{
        if(value.season=='2015')
        {
            return value.id;
        }
    }).map((item) => {
        return item.id
     })
     
     let top=deliveriesFile.reduce((accumulator,current)=>{
      
        if(matchId.includes(current.match_id)){
              //  if(accumulator[current.bowler]){
              //   accumulator[current.ball]++
              //     accumulator[current.bowler]+=parseInt(current.total_runs)
              //  }
              //  else{
              //   accumulator[current.ball]=1
              //   accumulator[current.bowler]=parseInt(current.total_runs);
              //  }
              if(accumulator[current.bowler]){
                  accumulator[current.bowler].total_runs+=parseInt(current.total_runs);
                    accumulator[current.bowler].balls+=1
                    accumulator[current.bowler].strike=parseInt(accumulator[current.bowler].total_runs/(accumulator[current.bowler].balls/6))
                }
               
              else{
                accumulator[current.bowler]={}
                accumulator[current.bowler].total_runs=parseInt(current.total_runs);
                accumulator[current.bowler].balls=1
                accumulator[current.bowler].strike=parseInt(accumulator[current.bowler].total_runs/(accumulator[current.bowler].balls/6))
               
            
              }
        }
        return accumulator
     },{})
     
     let result=Object.entries(top).sort(([key1,item1],[key2,item2])=>{
          //console.log(item1)
          return item1.strike-item2.strike
     }).slice(0,10)
     const object = Object.fromEntries(result);
    return object
     
}