let fs = require('fs')
const csvtojson = require('csvtojson');
const path = require('path');
let matchesPath = path.resolve(__dirname,'../data/matches.csv');
let deliveriesPath = path.resolve(__dirname,'../data/deliveries.csv');

csvtojson().fromFile(matchesPath).then((matchesPath)=>{

  csvtojson().fromFile(deliveriesPath).then((deliveriesFile)=>{
    let bowlerEconomyResult=bowlerEconomy(deliveriesFile);
    //console.log(deliveriesFile)

    
     console.log(bowlerEconomyResult);
     fs.writeFileSync('../public/output/9-bowler-best-economy-in-super-over.json', JSON.stringify(bowlerEconomyResult)); 
    
  })  
})
function bowlerEconomy(deliveriesFile){
    let Economy=deliveriesFile.reduce((accumulator,current)=>{
        if(current.is_super_over!=0){
            if(accumulator[current.bowler]){
                accumulator[current.bowler].total_runs+=parseInt(current.total_runs);
                  accumulator[current.bowler].balls+=1
                  accumulator[current.bowler].economy=parseInt(accumulator[current.bowler].total_runs/(accumulator[current.bowler].balls/6))
              }
             
            else{
              accumulator[current.bowler]={}
              accumulator[current.bowler].total_runs=parseInt(current.total_runs);
              accumulator[current.bowler].balls=1
              accumulator[current.bowler].economy=parseInt(accumulator[current.bowler].total_runs/(accumulator[current.bowler].balls/6))
             
          
            }
        }
        return accumulator
    },{})
    let result=Object.entries(Economy).sort(([key1,item1],[key2,item2])=>{
        //console.log(item1)
        return item1.economy-item2.economy
   }).slice(0,1)
   return result;
}